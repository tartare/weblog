# Weblog

This web application is to help analyzing log easily

* apache access log file
* apache error log files
* CSP reports

![Screenshot 0](screenshot0.png)

## Features

* All table's columns are sortable by just clicking on the column header.
* Access logs can hide localhost or home IP (edit setting.php file first)
* Add or remove log file(s) in setting.php file

It can be improve easily, by adding a class file which print the log in colorized lines or tables.

## Print apache access log

![Screenshot 1](screenshot1.png)

## Print apache error log

![Screenshot 2](screenshot2.png)
