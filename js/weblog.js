$(document).ready(function() {
  $('.menuentry').click(
    function() {
      getlog($(this).attr('class').replace('menuentry', '').replace(/\s/g, ''), $(this).attr('id'));
    });
});

function getlog(category, filename)
{
  $('#btncloselog').off( );
  $('td.tooltip').off( );
  $('td.tooltip').off( );
  $('div.tooltip').off( );
  $('#hide-localhost').off( );
  $('#show-localhost').off( );
  $('#hide-home').off( );
  $('#show-home').off( );
  $('#logcontent').html( '<div class="emptycontent"></div><div class="loading"></div>' );
  $.ajax({
    type: 'POST',
    url: "log.php",
    data: {
      "category": encodeURI(category),
      "filename": encodeURI(filename),
    },
    success: function( data ) {
      $('#logcontent').html( '<p>' + data + '</p>' );
      var sortable = document.getElementById('sortable');
      sorttable.makeSortable(sortable);
      $('#btncloselog').click(
        function() {
          closeLog();
      });
      $('td.tooltip').mouseover(
        function() {
          $(this).find("div").show();
      });
      $('td.tooltip').mouseout(
        function() {
          $(this).find("div").hide();
      });
      $('div.tooltip').click(
        function() {
          $(this).hide();
      });
      $('#hide-localhost').click(
        function() {
          $('#sortable > tbody  > tr.ip-localhost').each( function() { $(this).hide(); });
          $('#hide-localhost').addClass( 'button-reverse' );
          $('#show-localhost').removeClass( 'button-reverse' );
      });
      $('#show-localhost').click(
        function() {
          $('#sortable > tbody  > tr.ip-localhost').each( function() { $(this).show(); });
          $('#show-localhost').addClass( 'button-reverse' );
          $('#hide-localhost').removeClass( 'button-reverse' );
      });
      $('#hide-home').click(
        function() {
          $('#sortable > tbody  > tr.ip-home').each( function() { $(this).hide(); });
          $('#hide-home').addClass( 'button-reverse' );
          $('#show-home').removeClass( 'button-reverse' );
      });
      $('#show-home').click(
        function() {
          $('#sortable > tbody  > tr.ip-home').each( function() { $(this).show(); });
          $('#show-home').addClass( 'button-reverse' );
          $('#hide-home').removeClass( 'button-reverse' );
      });
      $('td.whois').click(
        function() {
          whois($(this));
      });
    },
    error: function( jqXHR ) {
      $('#logcontent').html( '<div class="error message">Error ' + jqXHR.responseJSON.code + ': ' + jqXHR.responseJSON.message + '<span class="right" id="btncloselog"></span></div><div class="emptycontent"></div>' );
      $('#btncloselog').click(
        function() {
          closeLog();
      });
    },
  });
}

function closeLog()
{
  $('#logcontent').html( '<div class="emptycontent"></div>' );
}

function whois(element)
{
  var host = $(element).html();
  $.ajax({
    type: 'POST',
    url: "whois.php",
    data: {
      "host": encodeURI(host),
    },
    success: function( data ) {
      $(element).append('<div class="tooltip"><pre>' + data.message + '</pre></div>');
      $(element).find("div").show();
      $(element).find("div").mouseout(
        function() {
          $(element).find("div").remove();
      });
    },
    error: function( jqXHR ) {
      $(element).append('<div class="tooltip">Error ' + jqXHR.responseJSON.code + ',' + jqXHR.responseJSON.message + '</div>');
      $(element).find("div").show();
      $(element).find("div").mouseout(
        function() {
          $(element).find("div").remove();
      });
    },
  });
}
