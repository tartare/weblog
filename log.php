<?php
  include_once( "config.php" );

  function exitError( $message, $code )
  {
      header( "HTTP/1.1 500 Internal Server Error" );
      header( 'Content-Type: application/json; charset=UTF-8' );
      die( json_encode( array( 'message' => $message, 'code' => $code ) ) );
  }
  
  spl_autoload_register(function ($name) {
    if ( file_exists( 'class/' . $name . '.class.php' ) ) {
        include 'class/' . $name . '.class.php';
    }
    else {
      throw new Exception( "Unable to load $name template ($name.class.php)" );
    }
  });
  //error_reporting(0);
  $warning = null;
  
  if ( isset( $_POST['filename'] ) and ! empty( $_POST['filename'] ) ) {
    $filename = urldecode( $_POST['filename'] );
  }
  if ( isset( $_POST['category'] ) and ! empty( $_POST['category'] ) ) {
    $category = urldecode( $_POST['category'] );
  }
  if ( isset( $filename ) ) {
    try {
      $log = new $category( $filename );
    }
    catch (Exception $e) {
      $warning = "<div class=\"warning message\">" . $e->getMessage() . ". Using PlainText.</div>";
    }
    if ( ! isset( $log ) ) {
      try {
        $log = new PlainText( $filename );
      }
      catch (Exception $e) {
        exitError( $e->getMessage(), 1 );
      }
    }
    $status = $log->checkLog();
    if( $status === 1 ) {
      exitError( "The file $filename was not found.", 2 );
    }
    elseif( $status === 2 ) {
      exitError( "The file $filename was not readable.", 3 );
    }
    
    $log->setIpWanAddresses( $IP_WAN_LOCAL, $IP_WAN_HOME );
    $text = $log->getLog();
    if ( $text === false ) {
      exitError( "The file $filename was not readable.", 4 );
    }
    header( "HTTP/1.1 200 OK" );
    header( 'Content-Type: text/html; charset=UTF-8' );
    echo "<p class=\"filename\">File $filename<span class=\"right\" id=\"btncloselog\"></span></p>";
    if ( isset ( $warning ) ) echo $warning;
    $text = $log->getLog();
    if ( strlen( $text ) ) {
      echo $log->getOptions();
      echo $text;
    }
    else {
      echo "<div class=\"emptycontent\"></div>";
    }
    exit( 0 );
  }
