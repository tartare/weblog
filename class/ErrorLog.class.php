<?php

class ErrorLog extends Table
{
  protected $_regex = '/^\[([^\]]+)\]\s+\[([^\]]+)\]\s+(?:\[pid\s+([^\]]+)\])?\s+(?:\[client\s+([^\]]+)\])?\s*(.*)$/i';
  
  protected $_tableHeaders = array(
    'Date',
    'Time',
    //'Timezone',
    'Module',
    'Severity',
    'PID',
    'IP Addr',
    'Message',
  );
  
  protected function _getTableBody()
  {
    $modsec = new ErrorModsec();
    $tbody = '<tbody>';
    foreach ( $this->_lines as $line ) {
      //$tbody .= "<!-- $line /-->";
      $rows = $this->_getRows( $line );
      if( $rows === false ) continue;
      $tbody .= '<tr>';
      list( $module, $status ) = explode( ':', $rows[2] );
      for( $i = 1 ; $i <= count( $rows ) ; $i++ ) {
        if ( $i == 1 ) {
          // Tue Nov 14 10:50:53.926732 2017
          $datetime = DateTime::createFromFormat('D M d H:i:s.u Y', $rows[$i]);
          $tbody .= "<td class=\"$status\">" . $datetime->format('d/m/Y') . "</td>";
          $tbody .= "<td class=\"$status\">" . $datetime->format('H:i:s') . "</td>";
        }
        elseif( $i == 2 ) {
          $tbody .= "<td class=\"$status\">$module</td>";
          $tbody .= "<td class=\"$status\">$status</td>";
        }
        elseif( $i == 5 ) {
          if ( preg_match( '/ModSecurity/', $rows[$i] ) ) {
            $tbody .= "<td class=\"$status tooltip\"><div class=\"tooltip\">" . $rows[$i] . "</div>" . $modsec->getTable( $rows[$i] ) . "</td>";
            //$tbody .= "<td class=\"$status tooltip\"><div class=\"tooltip\">" . $modsec->getTable( $rows[$i] ) . "</div>" . $rows[$i] . "</td>";
          }
          else {
            $tbody .= "<td class=\"$status\">" . $rows[$i] . "</td>";
          }
        }
        else {
          $tbody .= "<td class=\"$status\">" . $rows[$i] . "</td>";
        }
      }
      $tbody .= '</tr>';
    }
    $tbody .= '</tbody>';
    return $tbody;
  }
};
