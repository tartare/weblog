<?php

class PlainText
{
  protected $_filename;
  
  protected $_ip_wan_local = array();
  protected $_ip_wan_home = array();
  
  public function __construct( $filename )
  {
    $this->_filename = $filename;
  }
  
  public function checkLog()
  {
    if ( ! is_file( $this->_filename ) ) {
      return 1;
    }
    if ( ! is_readable( $this->_filename ) ) {
      return 2;
    }
    return 0;
  }
  
  public function setIpWanAddresses( $local, $home )
  {
    $this->_ip_wan_local = $local;
    $this->_ip_wan_home = $home;
  }
  
  public function getLog()
  {
    $content = file_get_contents( $this->_filename );
    if ( is_bool( $content ) ) return false;
    return "<pre>$content</pre>";
  }
  
  public function getOptions()
  {
    return '';
  }
};
