<?php

class Table extends PlainText
{
  protected $_regex = "/\s+/";
  
  protected $_lines = array();
  
  protected $_tableHeaders = array();
  
  protected function _getRows( $line )
  {
    $matches = array();
    $rows = preg_match_all( $this->_regex, $line, $matches, PREG_SPLIT_DELIM_CAPTURE );
    if ( count( $matches ) ) {
      // Remove whole match from array
      unset( $matches[0][0] );
      return $matches[0];
    }
    return false;
  }
  
  protected function _getTableHeader()
  {
    $thead = '';
    if( ! count( $this->_tableHeaders ) ) {
      return $thead;
    }
    $thead .= '<thead>';
    $thead .= '<tr>';
    foreach ( $this->_tableHeaders as $header ) {
      $thead .= "<th>$header</th>";
    }
    $thead .= '</tr>';
    $thead .= '</thead>';
    return $thead;
  }
  
  protected function _getTableBody()
  {
    $tbody = '<tbody>';
    foreach ( $lines as $line ) {
      $tbody .= '<tr>';
      $rows = $this->_getRows( $line );
      foreach( $rows as $row ) {
        $tbody .= "<td>$row</td>";
      }
      $tbody .= '</tr>';
    }
    $tbody .= '</tbody>';
    return $tbody;
  }
  
  public function getLog()
  {
    $content = file_get_contents( $this->_filename );
    if ( is_bool( $content ) ) return false;
    $this->_lines = preg_split( "/\n/", $content );
    $table = "";
    if( ! count( $this->_lines ) or ( count( $this->_lines ) == 1 and preg_match( '/^\s*$/', $this->_lines[0] ) ) ) return $table;
    $table .= '<table class="sortable" id="sortable">';
    $table .= $this->_getTableHeader();
    $table .= $this->_getTableBody();
    $table .= '</table>';
    return $table;
  }  
};
