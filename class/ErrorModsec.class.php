<?php

class ErrorModsec
{
  protected $_regex = '/^(?:\[client ([^\]]+)\])?.*ModSecurity:\s+(.*)\s*\[file "(\S+)"\]\s*\[line "(\S+)"\]\s*(.*)\s*\[hostname "([^\]]+)"\]\s*\[uri "([^\]]+)"\]\s*\[unique_id "(\S+)"\]\s*$/';
   
  public function getTable( $line )
  {
    if ( preg_match( $this->_regex, $line, $matches ) ) {
      $optdata = array( 'id', 'rev', 'data', 'msg', 'severity', 'ver', 'maturity', 'accuracy' );
      foreach( $optdata as $tag ) {
        if ( preg_match( "/\[$tag \"([^\]]+)\"\]/", $matches[5], $submatches ) ) {
          $modsec[$tag] = $submatches[1];
        }
      }
      if ( preg_match_all( "/\[tag \"([^\]]+)\"\]/", $matches[5], $submatches ) ) {
        unset( $submatches[0] );
        $modsec['tag'] = join( ", ", $submatches[1] );
      }
      if( isset( $matches[2] ) ) $content = $matches[2];
      $content .= "<table class=\"subtable\">";
      $content .= "<th>File</th><td>";
      if( isset( $matches[3] ) ) $content .= $matches[3];
      $content .= "</td>";
      $content .= "<th>Revision</th><td>";
      if( isset( $modsec['rev'] ) ) $content .= $modsec['rev'];
      $content .= "</td>";
      $content .= "<tr></tr>";
      $content .= "<th>Line</th><td>";
      if( isset( $matches[4] ) ) $content .= $matches[4];
      $content .= "</td>";
      $content .= "<th>Severity</th><td>";
      if( isset( $modsec['severity'] ) ) $content .= $modsec['severity'];
      $content .= "</td>";
      $content .= "<tr></tr>";
      $content .= "<th>Message</th><td>";
      if( isset( $matches[7] ) ) $content .= $matches[7];
      $content .= "</td>";
      $content .= "<th>Version</th><td>";
      if( isset( $modsec['ver'] ) ) $content .= $modsec['ver'];
      $content .= "</td>";
      $content .= "<tr></tr>";
      $content .= "<th>ID</th><td>";
      if( isset( $modsec['id'] ) ) $content .= $modsec['id'];
      $content .= "</td>";
      $content .= "<th>Maturity</th><td>";
      if( isset( $modsec['maturity'] ) ) $content .= $modsec['maturity'];
      $content .= "</td>";
      $content .= "<tr></tr>";
      $content .= "<th>Hostname</th><td>";
      if( isset( $matches[6] ) ) $content .= $matches[6] . "</td>";
      $content .= "<th>Accuracy</th><td>";
      if( isset( $modsec['accuracy'] ) ) $content .= $modsec['accuracy'];
      $content .= "</td>";
      $content .= "<tr></tr>";
      $content .= "<th>URI</th><td>";
      if( isset( $matches[7] ) ) $content .= $matches[7];
      $content .= "</td>";
      $content .= "<th>Data</th><td>";
      if( isset( $modsec['data'] ) ) $content .= $modsec['data'];
      $content .= "</td>";
      $content .= "<tr></tr>";
      $content .= "<th>Unique ID</th><td>";
      if( isset( $matches[8] ) ) $content .= $matches[8];
      $content .= "</td>";
      $content .= "<th>Client</th><td>";
      if( isset( $matches[1] ) ) $content .= $matches[1];
      $content .= "</td>";
      $content .= "<tr></tr>";
      $content .= "<th>Tag(s)</th><td colspan=\"3\">";
      if( isset( $modsec['tag'] ) ) $content .= $modsec['tag'];
      $content .= "</td></tr>";
      $content .= "</table>";
      return $content;
    }
    else {
      return $line;
    }
  }
};
