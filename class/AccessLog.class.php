<?php

class AccessLog extends Table
{
  protected $_regex = '/^(\S+)\s+(\S+)\s+(\S+)\s+\[([^:]+):(\d+:\d+:\d+)\s+([^\]]+)\]\s+\"(\S+\s*.*?\s*\S*)\"\s+(\S+)\s+(\S+)\s*"?([^"]*)?"?\s*"?([^"]*)?"?$/';
  
  protected $_tableHeaders = array(
    'IP Addr',
    'Remote Logname',
    'Remote User',
    'Date',
    'Time',
    'Timezone',
    'Method',
    'URL',
    'Protocol',
    'Status Code',
    'Bytes',
    'Referer',
    'User Agent',
  );
  
  protected function _getTableBody()
  {
    $httpcode = new HttpCode();
    $tbody = '<tbody>';
    foreach ( $this->_lines as $line ) {
      $rows = $this->_getRows( $line );
      if( ! count( $rows ) ) continue;
      if( $rows === false ) continue;
      if ( $rows[1] == "::1" || $rows[1] == "127.0.0.1" ) {
        $tbody .= "<tr class=\"ip-localhost\">";
      }
      elseif( isset( $this->_ip_wan_local['v4'] ) && $rows[1] == $this->_ip_wan_local['v4'] ) {
        $tbody .= "<tr class=\"ip-localhost\">";
      }
      elseif( isset( $this->_ip_wan_local['v6'] ) && $rows[1] == $this->_ip_wan_local['v6'] ) {
        $tbody .= "<tr class=\"ip-localhost\">";
      }
      elseif( isset( $this->_ip_wan_home['v4'] ) && $rows[1] == $this->_ip_wan_home['v4'] ) {
        $tbody .= "<tr class=\"ip-home\">";
      }
      elseif( isset( $this->_ip_wan_home['v6'] ) && $rows[1] == $this->_ip_wan_home['v6'] ) {
        $tbody .= "<tr class=\"ip-home\">";
      }
      else {
        $tbody .= "<tr>";
      }
      $status = 'error';
      $status_code = (int)( $rows[8] );
      if ( $status_code < 200 ) {
          $status = 'notice';
      }
      elseif ( $status_code >= 200 && $status_code < 300 ) {
          $status = 'ok';
      }
      elseif ( $status_code >= 300 && $status_code < 400 ) {
          $status = 'notice';
      }
      for( $i = 1 ; $i <= count( $rows ) ; $i++ ) {
        if ( $i == 1 ) {
          $tbody .= "<td class=\"$status whois\">" . $rows[$i] . "</td>";          
        }
        elseif ( $i == 4 ) {
          // 
          $datetime = DateTime::createFromFormat('d/M/Y', $rows[$i]);
          $tbody .= "<td class=\"$status\">" . $datetime->format('d/m/Y') . "</td>";
        }
        elseif ( $i == 7 ) {
          if( $rows[$i] != "-" ) {
            list( $method, $url, $protocol ) = explode( " ", $rows[$i] );
          }
          else {
            $method = "-";
            $url = "";
            $protocol = "";
          }
          $tbody .= "<td class=\"$status\">$method</td>";
          $tbody .= "<td class=\"$status\">$url</td>";
          $tbody .= "<td class=\"$status\">$protocol</td>";
        }
        elseif ( $i == 8 ) {
          $message = '<strong>' . $httpcode->getStatusByCode( htmlspecialchars( $rows[$i] ) ) . '</strong>';
          $message .= '<br/><br/>';
          $message .= $httpcode->getMessageByCode( htmlspecialchars( $rows[$i] ) );
          $tbody .= "<td class=\"$status tooltip\"><div class=\"tooltip\">$message</div>" . $rows[$i] . "</td>";
        }
        else {
          $tbody .= "<td class=\"$status\">" . $rows[$i] . "</td>";
        }
      }
      $tbody .= '</tr>';
    }
    $tbody .= '</tbody>';
    return $tbody;
  }
  
  public function getOptions()
  {
    $opts = "<div class=\"buttons\">";
    $opts .= "<span class=\"button\" id=\"hide-localhost\">Hide localhost</span>";
    $opts .= "<span class=\"button button-reverse\" id=\"show-localhost\">Show localhost</span>";
    $opts .= "<span class=\"button\" id=\"hide-home\">Hide Home</span>";
    $opts .= "<span class=\"button button-reverse\" id=\"show-home\">Show Home</span>";
    $opts .= "</div>";
    return $opts;
  }
};
