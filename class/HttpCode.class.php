<?php

class HttpCode
{
  protected $_code;
  
  protected $_codeEN = array(
    100 => array(
      'status'  => "Continue",
      'message' => "Waiting for the end part of the request.\nThe initial part of the request has been received and the client may continue with its request.",
    ),
    101 => array(
      'status'  => "Switching Protocols",
      'message' => "The server is complying with a client request to switch protocols to the one specified in the Upgrade header field.\nThe client asked the server to use another protocol than the one actually used, and the server complied this request.",
    ),
    102 => array(
      'status'  => "Processing",
      'message' => "A WebDAV request may contain many sub-requests involving file operations, requiring a long time to complete the request.\nThis code indicates that the server has received and is processing the request, but no response is available yet. This prevents the client from timing out and assuming the request was lost..",
    ),
    200 => array(
      'status'  => "OK",
      'message' => "The HTTP request has succeeded.\nThe information returned with the response is dependent on the method used in the request. For example the response to a standard GET request issued by a web browser is the requested resource (i.e. an HTML page, an image, etc).",
    ),    
    201 => array(
      'status'  => "Created",
      'message' => "The request has been fulfilled and resulted in a new resource being created._nThe newly created resource can be referenced by the URI(s) returned in the entity of the response, with the most specific URL for the resource given by a Location header field.",
    ),
    202 => array(
      'status'  => "Accepted",
      'message' => "The request has been accepted for processing, but the processing has not been completed.\nThis code is used instead of 201 when the processing of the request cannot be carried out immediately, leaving the result undetermined.",
    ),
    203 => array(
      'status'  => "Non-Authoritative Information",
      'message' => "Usually the preliminary information sent from a server to a browser comes directly from the server. If it does not, then this code might also be sent to indicate that information did not come from a known source.",
    ),
    204 => array(
      'status'  => "No Content",
      'message' => "The request was accepted and filled but no new information is being sent back.\nThe browser receiving this response should not change its screen display (although new, and changed, private header information may be sent).",
    ),
    205 => array(
      'status'  => "Reset Content",
      'message' => "The browser should clear the form used for this transaction for additional input.\nAppropriate for data-entry CGI applications.",
    ),
    206 => array(
      'status'  => "Partial Content",
      'message' => "The server is returning partial data of the size requested.\nUsed in response to a request specifying a Range header. The server must specify the range included in the response with the Content-Range header.",
    ),
    207 => array(
      'status'  => "Multi-Status",
      'message' => "The message body that follows is an XML message and can contain a number of separate response codes, depending on how many sub-requests were made.",
    ),
    208 => array(
      'status'  => "Already Reported",
      'message' => "The members of a DAV binding have already been enumerated in a preceding part of the (multistatus) response, and are not being included again.",
    ),
    226 => array(
      'status'  => "IM Used",
      'message' => "The server has fulfilled a request for the resource, and the response is a representation of the result of one or more instance-manipulations applied to the current instance.",
    ),
    300 => array(
      'status'  => "Multiple Choices",
      'message' => "The requested URI refers to more than one resource.\nFor example, the URI could refer to a document that has been translated into many languages. The entity body returned by the server could have a list of more specific data about how to choose the correct resource.",
    ),

    301 => array(
      'status'  => "Moved Permanently",
      'message' => "The requested resource has been assigned a new permanent address (URI).\nAny future references to this resource should be done using one of the returned URIs. Web browsers should automatically load the requested resource using its new address.",
    ),
    302 => array(
      'status'  => "Moved Temporarily",
      'message' => "The requested resource resides temporarily under a different URI.\nSince the redirection may be altered on occasion, the client should continue to use the Request-URI for future requests.",
    ),
    303 => array(
      'status'  => "See Other",
      'message' => "The requested URI can be found at a different URI (specified in the Location header) and should be retrieved by a GET on that resource.",
    ),
    304 => array(
      'status'  => "Not Modified",
      'message' => "The web browser has performed a conditional GET request and access is allowed, but the document has not been modified.\nThis classic response means you have configured your web browser to use an HTTP cache (proxy) in which a copy of the requested document is already stored. The cache proxy thus asked the server if the original document was modified, and received this response, so it will use the local copy instead of loading it from the server.",
    ),
    305 => array(
      'status'  => "Use Proxy",
      'message' => "The requested URI must be accessed through the proxy in the Location header.",
    ),
    306 => array(
      'status'  => "Switch Proxy",
      'message' => "No longer used. Originally meant \"Subsequent requests should use the specified proxy\".",
    ),
    307 => array(
      'status'  => "Temporary Redirect",
      'message' => "In this case, the request should be repeated with another URI; however, future requests should still use the original URI. In contrast to how 302 was historically implemented, the request method is not allowed to be changed when reissuing the original request. For example, a POST request should be repeated using another POST request.",
    ),
    308 => array(
      'status'  => "Permanent Redirect",
      'message' => "The request and all future requests should be repeated using another URI. 307 and 308 parallel the behaviors of 302 and 301, but do not allow the HTTP method to change. So, for example, submitting a form to a permanently redirected resource may continue smoothly.",
    ),
    400 => array(
      'status'  => "Bad Request",
      'message' => "The HTTP request could not be understood by the server due to malformed syntax.\nThe web browser may be too recent, or the HTTP server may be too old.",
    ),
    401 => array(
      'status'  => "Unauthorized",
      'message' => "The request requires user authentication.\nThis means that all or a part of the requested server is protected by a password that should be given to the server to allow access to its contents.",
    ),
    402 => array(
      'status'  => "Payment Required",
      'message' => "This code is not yet implemented in HTTP.",
    ),
    403 => array(
      'status'  => "Forbidden",
      'message' => "The HTTP server understood the request, but is refusing to fulfill it.\nThis status code is commonly used when the server does not wish to reveal exactly why the request has been refused, or when no other response is applicable (for example the server is an Intranet and only the LAN machines are authorized to connect).",
    ),
    404 => array(
      'status'  => "Not Found",
      'message' => "The server has not found anything matching the requested address (URI) ( not found ).\nThis means the URL you have typed or cliked on is wrong or obsolete and does not match any document existing on the server (you may try to gradualy remove the URL components from the right to the left to eventualy retrieve an existing path).",
    ),
    405 => array(
      'status'  => "Method Not Allowed",
      'message' => "This code is given with the Allow header and indicates that the method used by the client is not supported for this URI.",
    ),
    406 => array(
      'status'  => "Not Acceptable",
      'message' => "The URI specified by the client exists, but not in a format preferred by the client.\nAlong with this code, the server provides the Content-Language, Content-Encoding, and Content-Type headers.",
    ),
    407 => array(
      'status'  => "Proxy Authentication Required",
      'message' => "The proxy server needs to authorize the request before forwarding it.",
    ),
    408 => array(
      'status'  => "Request Time-out",
      'message' => "This response code means the client did not produce a full request within some predetermined time (usually specified in the server's configuration), and the server is disconnecting the network connection.",
    ),
    409 => array(
      'status'  => "Conflict",
      'message' => "This code indicates that the request conflicts with another request or with the server's configuration.\nInformation about the conflict should be returned in the data portion of the reply.",
    ),
    410 => array(
      'status'  => "Gone",
      'message' => "This code indicates that the requested URI no longer exists and has been permanently removed from the server.",
    ),
    411 => array(
      'status'  => "Length Required",
      'message' => "The server will not accept the request without a Content-Length header supplied in the request.",
    ),
    412 => array(
      'status'  => "Precondition Failed",
      'message' => "The condition specified by one or more If... headers in the request evaluated to false.",
    ),
    413 => array(
      'status'  => "Request Entity Too Large",
      'message' => "The server will not process the request because its entity body is too large.",
    ),
    414 => array(
      'status'  => "Request-URI Too Long",
      'message' => "The server will not process the request because its request URI is too large.",
    ),
    415 => array(
      'status'  => "Unsupported Media Type",
      'message' => "The server will not process the request because its entity body is in an unsupported format.",
    ),
    416 => array(
      'status'  => "Requested range unsatifiable",
      'message' => "The server will not process the request because the requested range is invalid.",
    ),
    417 => array(
      'status'  => "Expectation failed",
      'message' => "The behavior expected fot the server is not supported.",
    ),
    418 => array(
      'status'  => "I'm a teapot",
      'message' => "This code was defined in 1998 as one of the traditional IETF April Fools' jokes, in RFC 2324, Hyper Text Coffee Pot Control Protocol, and is not expected to be implemented by actual HTTP servers. The RFC specifies this code should be returned by teapots requested to brew coffee. This HTTP status is used as an Easter egg in some websites, including Google.com.",
    ),
    421 => array(
      'status'  => "Misdirected Request",
      'message' => "The request was directed at a server that is not able to produce a response (for example because of a connection reuse).",
    ),
    422 => array(
      'status'  => "Unprocessable Entity",
      'message' => "The request was well-formed but was unable to be followed due to semantic errors.",
    ),
    423 => array(
      'status'  => "Locked",
      'message' => "The resource that is being accessed is locked.",
    ),
    424 => array(
      'status'  => "Failed Dependency",
      'message' => "The request failed due to failure of a previous request (e.g., a PROPPATCH).",
    ),
    426 => array(
      'status'  => "Upgrade Required",
      'message' => "The client should switch to a different protocol such as TLS/1.0, given in the Upgrade header field.",
    ),
    428 => array(
      'status'  => "Precondition Required",
      'message' => "The origin server requires the request to be conditional. Intended to prevent the 'lost update' problem, where a client GETs a resource's state, modifies it, and PUTs it back to the server, when meanwhile a third party has modified the state on the server, leading to a conflict.",
    ),
    429 => array(
      'status'  => "Too Many Requests",
      'message' => "The user has sent too many requests in a given amount of time. Intended for use with rate-limiting schemes.",
    ),
    431 => array(
      'status'  => "Request Header Fields Too Large",
      'message' => "The server is unwilling to process the request because either an individual header field, or all the header fields collectively, are too large.",
    ),
    451 => array(
      'status'  => "Unavailable For Legal Reasons",
      'message' => "A server operator has received a legal demand to deny access to a resource or to a set of resources that includes the requested resource.The code 451 was chosen as a reference to the novel Fahrenheit 451.",
    ),
    500 => array(
      'status'  => "Internal Server Error",
      'message' => "The HTTP server encountered an unexpected condition which prevented it from fulfilling the request.\nFor example this error can be caused by a serveur misconfiguration, or a resource exhausted or denied to the server on the host machine.",
    ),
    501 => array(
      'status'  => "Not Implemented",
      'message' => "The HTTP server does not support the functionality required to fulfill the request.\nThis is the appropriate response when the server does not recognize the request method and is not capable of supporting it for any resource (either the web browser is too recent, or the HTTP server is too old).",
    ),
    502 => array(
      'status'  => "Bad Gateway",
      'message' => "The gateway server returned an invalid response.\nThe HTTP server, while acting as a gateway or proxy, received an invalid response from the upstream server it accessed in attempting to fulfill the request.",
    ),
    503 => array(
      'status'  => "Service Unavailable",
      'message' => "The HTTP server is currently unable to handle the request due to a temporary overloading or maintenance of the server.\nThe implication is that this is a temporary condition which will be alleviated after some delay.",
    ),
    504 => array(
      'status'  => "Gateway Time-out",
      'message' => "This response is like 408 (Request Time-out) except that a gateway or proxy has timed out.",
    ),
    505 => array(
      'status'  => "HTTP Version not supported",
      'message' => "The server will not support the HTTP protocol version used in the request.",
    ),
    506 => array(
      'status'  => "Variant Also Negotiates",
      'message' => "Transparent content negotiation for the request results in a circular reference.",
    ),
    507 => array(
      'status'  => "Insufficient Storage",
      'message' => "The server is unable to store the representation needed to complete the request.",
    ),
    508 => array(
      'status'  => "Loop Detected",
      'message' => "The server detected an infinite loop while processing the request (sent in lieu of 208 Already Reported).",
    ),
    510 => array(
      'status'  => "Not Extended",
      'message' => "Further extensions to the request are required for the server to fulfil it.",
    ),
    511 => array(
      'status'  => "Network Authentication Required",
      'message' => "The client needs to authenticate to gain network access. Intended for use by intercepting proxies used to control access to the network (e.g., \"captive portals\" used to require agreement to Terms of Service before granting full Internet access via a Wi-Fi hotspot)",
    ),
  );
  
  protected $_codeFR = array(
    100 => array(
      'status'  => "Continuer",
      'message' => 'Attente de la fin de la requête.\nLa partie initiale de la requête a bien été reçue et le client peut continuer avec la fin de sa requête.'
    ),
    101 => array(
      'status'  => "Changement de protocole",
      'message' => "Le serveur accepte la requête du client de changer de protocole.\nLe client a demandé au serveur d'utiliser un autre protocole que celui actuellement utilisé, et le serveur accepte cette requête.",
    ),
    200 => array(
      'status'  => "OK",
      'message' => "La requête HTTP a été traitée avec succès.\nL'information retournée avec la réponse dépend de la méthode utilisée dans la requête. Par exemple la réponse à une requête GET classiquement émise par un navigateur web sera la ressource demandée (c'est-à-dire une page HTML, une image, etc ...).",
    ),    
    201 => array(
      'status'  => "Créé",
      'message' => "La requête a été correctement traitée et a résulté en la création d'une nouvelle ressource.\nCette ressource peut être référencée par l'URI retournée dans le corps de la réponse, avec l'URL la plus précise pour la ressource indiquée dans l'en-tête du champ \"Location\".",
    ),
    202 => array(
      'status'  => "Accepté",
      'message' => "La requête a été acceptée pour traitement, mais son traitement peut ne pas avoir abouti.\nCe code est utilisé en remplacement du 201 lorsque le traitement ne peut pas avoir lieu immédiatement, son résultat est donc indéterminé.",
    ),
    203 => array(
      'status'  => "Information non certifiée",
      'message' => "L'information retournée n'a pas été générée par le serveur HTTP mais par une autre source non authentifiée.",
    ),
    204 => array(
      'status'  => "Pas de contenu",
      'message' => "Le serveur HTTP a correctement traité la requête mais il n'y a pas d'information à envoyer en retour.\nCela peut par exemple se produire lorsqu'un fichier HTML ou le résultat d'un programme CGI-BIN est vide.",
    ),
    205 => array(
      'status'  => "Contenu réinitialisé",
      'message' => "Le client doit remettre à zéro le formulaire utilisé dans cette transaction.\nCe code est envoyé au logiciel de navigation quand il doit réinitialiser un formulaire généré dynamiquement par un CGI-BIN, par exemple.",
    ),
    206 => array(
      'status'  => "Contenu partiel",
      'message' => "Le serveur retourne une partie seulement de la taille demandée.\nCe code est utilisé lorsqu'une requête spécifiant une taille a été transmise.",
    ),
    300 => array(
      'status'  => "Choix multiples",
      'message' => "L'URI demandée concerne plus d'une ressource.\nPar exemple, l'URI concerne un document qui a été traduit en plusieurs langues. Le serveur doit retourner des informations indiquant comment choisir une ressource précise.",
    ),
    301 => array(
      'status'  => "Changement d'adresse définitif",
      'message' => "La ressource demandée possède une nouvelle adresse (URI).\nToute référence future à cette ressource doit être faite en utilisant l'une des URIs retournées dans la réponse. Le navigateur web doit normalement charger automatiquement la ressource demandée à sa nouvelle adresse.",
    ),
    302 => array(
      'status'  => "Changement d'adresse temporaire",
      'message' => "La ressource demandée réside temporairement à une adresse (URI) différente.\nCette re-direction étant temporaire, le navigateur web doit continuer à utiliser l'URI originale pour les requêtes futures.",
    ),
    303 => array(
      'status'  => "Voir ailleurs",
      'message' => "L'URI spécifié est disponible à un autre URI et doit être demandé par un GET.",
    ),
    304 => array(
      'status'  => "Non modifié",
      'message' => "Le navigateur web a effectué une requête GET conditionnelle et l'accès est autorisé, mais le document n'a pas été modifié.\nCette réponse classique signifie que vous avez configuré votre navigateur pour utiliser un cache HTTP (proxy) dans lequel une copie du document demandé est déjà stockée. Le proxy a donc demandé au serveur si le document original a changé depuis, et a reçu cette réponse : il pourra ainsi utiliser la copie locale.",
    ),
    305 => array(
      'status'  => "Utiliser le proxy",
      'message' => "L'URI spécifié doit être accédé en passant par le proxy.",
    ),
    400 => array(
      'status'  => "Mauvaise requête",
      'message' => "La requête HTTP n'a pas pu être comprise par le serveur en raison d'une syntaxe erronée.\nLe problème peut provenir d'un navigateur web trop récent ou d'un serveur HTTP trop ancien.",
    ),
    401 => array(
      'status'  => "Non autorisé",
      'message' => "La requête nécessite une identification de l'utilisateur.\nConcrètement, cela signifie que tout ou partie du serveur contacté est protégé par un mot de passe, qu'il faut indiquer au serveur pour pouvoir accéder à son contenu.",
    ),
    402 => array(
      'status'  => "Paiement exigé",
      'message' => "Ce code n'est pas encore mis en oeuvre dans le protocole HTTP.",
    ),
    403 => array(
      'status'  => "Interdit",
      'message' => "Le serveur HTTP a compris la requête, mais refuse de la traiter.\nCe code est généralement utilisé lorsqu'un serveur ne souhaite pas indiquer pourquoi la requête a été rejetée, ou lorsque aucune autre réponse ne correspond (par exemple le serveur est un Intranet et seules les machines du réseau local sont autorisées à se connecter au serveur).",
    ),
    404 => array(
      'status'  => "Non trouvé",
      'message' => "Le serveur n'a rien trouvé qui corresponde à l'adresse (URI) demandée ( non trouvé ).\nCela signifie que l'URL que vous avez tapée ou cliquée est mauvaise ou obsolète et ne correspond à aucun document existant sur le serveur (vous pouvez essayez de supprimer progressivement les composants de l'URL en partant de la fin pour éventuellement retrouver un chemin d'accès existant).",
    ),
    405 => array(
      'status'  => "Méthode non autorisée",
      'message' => "Ce code indique que la méthode utilisée par le client n'est pas supportée pour cet URI.",
    ),
    406 => array(
      'status'  => "Aucun disponible",
      'message' => "L'adresse (URI) spécifiée existe, mais pas dans le format préféré du client.\nLe serveur indique en retour le langage et les types d'encodages disponibles pour cette adresse.",
    ),
    407 => array(
      'status'  => "Authentification proxy exigée",
      'message' => "Le serveur proxy exige une authentification du client avant de transmettre la requête.",
    ),
    408 => array(
      'status'  => "Requête hors-délai",
      'message' => "Le client n'a pas présenté une requête complète pendant le délai maximal qui lui était imparti, et le serveur a abandonné la connexion.",
    ),
    409 => array(
      'status'  => "Conflit",
      'message' => "La requête entre en conflit avec une autre requête ou avec la configuration du serveur.\nDes informations sur les raisons de ce conflit doivent être indiquée en retour.",
    ),
    410 => array(
      'status'  => "Parti",
      'message' => "L'adresse (URI) demandée n'existe plus et a été définitivement supprimée du serveur.",
    ),
    411 => array(
      'status'  => "Longueur exigée",
      'message' => "Le serveur a besoin de connaître la taille de cette requête pour pouvoir y répondre.",
    ),
    412 => array(
      'status'  => "Précondition échouée",
      'message' => "Les conditions spécifiées dans la requête ne sont pas remplies.",
    ),
    413 => array(
      'status'  => "Corps de requête trop grand",
      'message' => "Le serveur ne peut traiter la requête car la taille de son contenu est trop importante.",
    ),
    414 => array(
      'status'  => "URI trop long",
      'message' => "Le serveur ne peut traiter la requête car la taille de l'objet (URI) a retourner est trop importante.",
    ),
    415 => array(
      'status'  => "Format non supporté 	Le serveur ne peut traiter la requête car son contenu est écrit dans un format non supporté.",
    ),
    416 => array(
      'status'  => "Plage demandée invalide 	Le sous-ensemble de recherche spécifié est invalide.",
    ),
    417 => array(
      'status'  => "Comportement erroné",
      'message' => "Le comportement prévu pour le serveur n'est pas supporté.",
    ),
    500 => array(
      'status'  => "Erreur interne du serveur",
      'message' => "Le serveur HTTP a rencontré une condition inattendue qui l'a empêché de traiter la requête.\nCette erreur peut par exemple être le résultat d'une mauvaise configuration du serveur, ou d'une ressource épuisée ou refusée au serveur sur la machine hôte.",
    ),
    501 => array(
      'status'  => "Non implémenté",
      'message' => "Le serveur HTTP ne supporte pas la fonctionnalité nécessaire pour traiter la requête.\nC'est la réponse émise lorsque le serveur ne reconnaît pas la méthode indiquée dans la requête et n'est capable de la mettre en oeuvre pour aucune ressource (soit le navigateur web est trop récent, soit le serveur HTTP est trop ancien).",
    ),
    502 => array(
      'status'  => "Mauvais intermédiaire",
      'message' => "Le serveur intermédiaire a fourni une réponse invalide.\nLe serveur HTTP a agi en tant qu'intermédiaire (passerelle ou proxy) avec un autre serveur, et a reçu de ce dernier une réponse invalide en essayant de traiter la requête.",
    ),
    503 => array(
      'status'  => "Service indisponible",
      'message' => "Le serveur HTTP est actuellement incapable de traiter la requête en raison d'une surcharge temporaire ou d'une opération de maintenance.\nCela sous-entend l'existence d'une condition temporaire qui sera levée après un certain délai.",
    ),
    504 => array(
      'status'  => "Intermédiaire hors-délai",
      'message' => "Cette réponse est identique au code 408 (requête hors-délai), mais ici c'est un proxy ou un autre intermédiaire qui a répondu hors délai.",
    ),
    505 => array(
      'status'  => "Version HTTP non supportée",
      'message' => "La version du protocole HTTP utilisée dans cette requête n'est pas (ou plus) supportée par le serveur.",
    ),
  );
  
  public function __construct( $language = "en" )
  {
    if ( strtolower( $language ) == 'fr' ) {
      $this->_code = &$this->_codeFR;
    }
    else {
      $this->_code = &$this->_codeEN;
    }
  }
  
  public function getMessageByCode( $code ) {
    return $this->_code[$code]['message'];
  }
  
  public function getStatusByCode( $code ) {
    return $this->_code[$code]['status'];
  }
};
