<?php

class CspLog extends Json
{  
  protected $_tableHeaders = array(
    'Date',
    'Time',
    'IP Addr',
    'Blocked Uri',
    'Document Uri',
    'Line Number',
    'Referrer',
    'Script Sample',
    'Status Code',
    'Disposition',
    'Violated Directive',
    'Effective Directive',
    'Source File',
    'Original Policy',
  );

  protected function _getTableBody()
  {
    $tbody = '<tbody>';
    foreach ( $this->_lines as $line ) {
      if( strlen( $line ) <= 1 ) continue;
      $rows = $this->_getRows( $line );  
      if( $rows === false ) continue;
      $tbody .= '<tr>';    
      foreach( $this->_tableHeaders as $header ) {
        $key = strtolower( str_replace( ' ', '-', $header ) );
        if( ! isset( $rows[$key] ) ) {
          $tbody .= "<td class=\"warning\"></td>";
          continue;
        }
        if ( $key == "original-policy" ) {
          $tbody .= "<td class=\"warning tooltip\"><div class=\"tooltip\">" . $rows[$key] . "</div>" . substr( $rows[$key], 0, 35 ) . "...</td>";
        }
        else {
          $tbody .= "<td class=\"warning\">" . $rows[$key] . "</td>";
        }
        unset( $rows[$key] );
      }
      // Display missing element(s)
      foreach( $rows as $key => $row ) {
        $tbody .= "<td class=\"warning\">$key: $row</td>";
      }
      $tbody .= '</tr>';
    }
    $tbody .= '</tbody>';
    return $tbody;
  }
};
