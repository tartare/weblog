<?php

class Json extends Table
{  
  protected $_lines = array();
  
  protected $_tableHeaders = array();
  
  protected function _getRows( $line )
  {
    list( $date, $time, $addr, $json ) = explode( ' ', preg_replace( '/<!\[CDATA\[/', '&lt;![CDATA[', $line ), 4 );
    $return = json_decode( $json, true )['csp-report'];
    $return['date'] = $date;
    $return['time'] = $time;
    $return['ip-addr'] = $addr;
    return $return;
  }
};
