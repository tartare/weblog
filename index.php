<?php
  include_once( "config.php" );
?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="AUTHOR" content="Didier Fabert">
  <link rel="icon" type="image/x-icon" href="favicon.ico">
  <title>Webserver log analyzer</title>
  <link rel="stylesheet" href="css/screen.css">
  <script type='text/javascript' src='js/jquery-3.2.1.min.js'></script>
  <script type='text/javascript' src='js/sorttable.js'></script>
  <script type="text/javascript" src="js/weblog.js"></script>
</head>
<body>
  <header id="header">
    <div id="logo">
            <h1>Webserver log analyzer</h1>
    </div>
  </header>    
  <nav id="nav">
    <div class="innertube">
      <?php foreach( $LOGFILES as $category => $logs ):?>
        <h3><?php echo "$category";?></h3>
        <ul>
         <?php foreach( $logs as $log ):?>
          <li><a class='menuentry <?php echo "$category";?>' id='<?php echo "$log";?>' href='#'><?php echo basename($log);?></a></li>
        <?php endforeach;?>
      </ul>
      <?php endforeach;?>
    </div>
  </nav>
  <main>
    <div class="innertube" id="logcontent">
      <div class="emptycontent"></div>
    </div>
  </main>
</body>
</html>
