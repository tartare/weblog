<?php
  include_once( "config.php" );

  function atexit( $message, $code = 255, $cmd = '' )
  {
    if( $code ) {
      header( "HTTP/1.1 500 Internal Server Error" );
    }
    else {
      header( "HTTP/1.1 200 OK" );
    }
    header( 'Content-Type: application/json; charset=UTF-8' );
    echo json_encode( array( 'message' => $message, 'code' => $code ) ) . PHP_EOL;
    exit( $code );
  }
  //error_reporting(0);
  
  $host = "115.97.148.56"; //"51.254.66.51";
  
  if ( isset( $_POST['host'] ) and ! empty( $_POST['host'] ) ) {
    $host = urldecode( $_POST['host'] );
  }
    //atexit( "No host provided" );
  
  $output = array();
  $retval = 255;
  $cmd = "WHOIS_HIDE=1 /usr/bin/jwhois -s $host 2>&1";
  exec ( $cmd, $output, $retval );
  
  atexit( join( "\n", $output ), $retval, $cmd );
  
