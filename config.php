<?php

$IP_WAN_LOCAL = array( 
  'v4' => "51.254.66.51",
  'v6' => null,
);

$IP_WAN_HOME = array( 
  'v4' => "78.244.172.191",
  'v6' => "2a01:e34:ef4a:cbf0:1fbe:9520:6143:e2e",
);

$LOGFILES = array(
  "AccessLog" => array(
    "/var/log/httpd/access_log",
    "/var/log/httpd/ssl_access_log",
    "/home/didier/vcs/weblog/data/access_log",
    "/home/didier/vcs/weblog/data/blog.tartarefr.eu.access.log",
    "/home/didier/vcs/weblog/data/wiki.tartarefr.eu.access.log",
    "/home/didier/vcs/weblog/data/map.tartarefr.eu.access.log",
    "/home/didier/vcs/weblog/data/monitoring.tartarefr.eu.access.log",
    "/home/didier/vcs/weblog/data/report.tartarefr.eu.access.log",
    "/home/didier/vcs/weblog/data/wikiold.tartarefr.eu.access.log",
    "/home/didier/vcs/weblog/data/monip.tartarefr.eu.access.log",
    "/home/didier/vcs/weblog/data/nominatim.tartarefr.eu.access.log",
    "/home/didier/vcs/weblog/data/ssl_access_log",
  ),
  "ErrorLog" => array(
    "/var/log/httpd/error_log",
    "/var/log/httpd/ssl_error_log",
    "/home/didier/vcs/weblog/data/error_log",
    "/home/didier/vcs/weblog/data/blog.tartarefr.eu.error.log",
    "/home/didier/vcs/weblog/data/wiki.tartarefr.eu.error.log",
    "/home/didier/vcs/weblog/data/map.tartarefr.eu.error.log",
    "/home/didier/vcs/weblog/data/monitoring.tartarefr.eu.error.log",
    "/home/didier/vcs/weblog/data/report.tartarefr.eu.error.log",
    "/home/didier/vcs/weblog/data/wikiold.tartarefr.eu.error.log",
    "/home/didier/vcs/weblog/data/monip.tartarefr.eu.error.log",
    "/home/didier/vcs/weblog/data/nominatim.tartarefr.eu.error.log",
    "/home/didier/vcs/weblog/data/ssl_error_log",
  ),
  "Modsec" => array(
    "/home/didier/vcs/weblog/data/modsec_audit.log",
    "/home/didier/vcs/weblog/data/modsec_debug.log",
    "/home/didier/vcs/weblog/data/modsec.log",
  ),
  "CspLog" => array(
    "/home/didier/vcs/weblog/data/csp.log",
    "/home/didier/vcs/weblog/data/ect.log",
    "/home/didier/vcs/weblog/data/pkp.log",
  ),
);
   
